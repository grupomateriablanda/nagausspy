%chk=conf_02_freq.chk
# freq=raman B3LYP/6-311g(d,p) int=ultrafine pop=(nbo,full) scf=conver=9

conf_02_freq

-1 3
 C                   -2.127303      0.999602      1.633452
 H                   -1.683749      1.585889      2.423060
 C                   -2.510403     -0.304005      1.584930
 H                   -2.463726     -1.083474      2.325673
 C                   -2.773483      0.555467     -0.430747
 H                   -2.943789      0.654242     -1.494657
 N                   -2.307775      1.519532      0.364735
 N                   -2.912168     -0.561562      0.286019
 C                   -3.364734     -1.859623     -0.244603
 H                   -2.706500     -2.618806      0.184238
 H                   -3.194554     -1.833324     -1.321309
 C                   -4.827798     -2.142466      0.084568
 H                   -5.108112     -3.114719     -0.327397
 H                   -4.989706     -2.177231      1.164444
 H                   -5.489803     -1.384706     -0.342669
 C                   -1.901832      2.854560     -0.087388
 H                   -2.272337      3.599143      0.614897
 H                   -0.814860      2.894070     -0.131481
 H                   -2.317108      3.024313     -1.078956
 Ni                   1.486483     -0.015856     -0.483214
 N                    3.373719     -0.411825     -0.475758
 N                    0.161386      0.489608     -1.884364
 N                    0.556879     -1.545009      0.317885
 N                    1.188242      1.430594      0.811563
 C                    4.527018     -0.648460     -0.485409
 C                   -0.618217      0.729534     -2.726820
 C                    0.042433     -2.423460      0.896071
 C                    0.980933      2.170969      1.697128
 S                    6.121993     -0.972624     -0.516084
 S                   -1.780606      1.071858     -3.836015
 S                   -0.732798     -3.632621      1.697795
 S                    0.602727      3.211917      2.907210


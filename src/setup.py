from setuptools import setup


setup(name="nagausspy",
      version="0.1-alpha",
      description="Some basic tools to read Gaussian things",
      author="Hadrian Montes",
      author_email="hadrian.montes@usc.es",
      packages=["nagausspy"]
     )
